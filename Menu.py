from tkinter import *
import manager as M



fontDefault = 'arial 18 bold'
fontTitle = 'arial 30 bold'



class NewVenda:
    def __init__(self, master, *args, **kwargs):

        self.master = master
        self.heading = Label(master, text='Nova Venda', font=fontTitle)
        self.heading.grid(row=0, column=0)

        self.itemSelectValue = {'name':'Undefined'}

        #Add Produto
        self.Fr_addProd = LabelFrame(text='Pesquisa de produto')
        self.Fr_addProd.grid( row=1, column=0)

        self.lb_addProd_id = Label(self.Fr_addProd,text='ID')
        self.lb_addProd_id.grid(row=0, column=0)
        self.ip_addProd_id = Entry(self.Fr_addProd, width=4, font=fontDefault)
        self.ip_addProd_id.grid( row=1, column=0)

        self.lb_addProd_name = Label(self.Fr_addProd, text='Produto')
        self.lb_addProd_name.grid(row=0, column=1)
        self.ip_addProd_name = Entry(self.Fr_addProd, width=25, font=fontDefault)
        self.ip_addProd_name.grid(row=1, column=1)

        self.lb_addProd_brand = Label(self.Fr_addProd, text='Marca')
        self.lb_addProd_brand.grid(row=0, column=3)
        self.ip_addProd_brand = Entry(self.Fr_addProd, width=20, font=fontDefault)
        self.ip_addProd_brand.grid(row=1, column=3)

        self.resultFrame = Frame()
        self.resultFrame.grid(row=3, column=0, pady=5)

        self.scrollbar = Scrollbar(self.resultFrame)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.result = Listbox(self.resultFrame, yscrollcommand=self.scrollbar.set, width=90)
        self.result.pack(side=LEFT, fill=BOTH)

        self.addProd_search = Button(self.Fr_addProd, text=' buscar', command=self.makeListResult)
        self.addProd_search.grid(row=1, column=4)
######################################Pesquisa

        self.ok = Button(text=' Selecionar', command=self.ok)
        self.ok.grid(row=4, column=0)
        ##########
        self.SelectItem = Frame()

        ### Lista de Resultados

    def makeListResult(self):
        self.resultFrame.destroy()

        self.resultFrame = Frame()
        self.resultFrame.grid(row=3, column=0, pady=5)

        self.scrollbar = Scrollbar(self.resultFrame)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.result = Listbox(self.resultFrame,  yscrollcommand=self.scrollbar.set, width=90)
        self.result.pack(side=LEFT, fill=BOTH)


        self.scrollbar.config(command=self.result.yview)

        lista = M.get_all_estoque()

        for index in range(len(lista)):
            if self.ip_addProd_name.get().lower() in lista[index][1]:
                self.result.insert(END,f'{lista[index][0]} : {lista[index][1].strip().upper()}')

        self.result.selection_set(0)
    def ok(self):
        self.itemSelectValue = {}
        ID = self.result.curselection()[0]
        self.SelectItem.destroy()
        self.itemSelectValue = M.get_estoque(int(self.result.selection_get().split(':')[0]))

        self.SelectItem = Frame()
        self.SelectItem.grid(row=5, column=0, pady=5)

        self.id_item = Label(self.SelectItem, text='ID ', font=fontDefault)
        self.name_item = Label(self.SelectItem, text=f'Nome :', font=fontDefault)
        self.brand_item = Label(self.SelectItem, text='Marca :', font=fontDefault)
        self.price_item = Label(self.SelectItem, text='Preço : ', font=fontDefault)
        self.stock_item = Label(self.SelectItem, text='Estoque :', font=fontDefault)

        self.id_item.grid(row=1, column=0, pady=5)
        self.name_item.grid(row=2, column=0, pady=5)
        self.brand_item.grid(row=3, column=0, pady=5)
        self.price_item.grid(row=4, column=0, pady=5)
        self.stock_item.grid(row=5, column=0, pady=5)

        self.id_item_value = Label(self.SelectItem, text=f'{self.itemSelectValue["id"]}')
        self.name_item_value = Label(self.SelectItem, text=f'{self.itemSelectValue["item"]}')
        self.brand_item_value = Label(self.SelectItem, text=f'Undefined')
        self.price_item_value = Label(self.SelectItem, text=f'{self.itemSelectValue["valor_unitario"]}')
        self.stock_item_value = Label(self.SelectItem, text=f'{self.itemSelectValue["qtd"]}')

        self.id_item_value.grid(row=1, column=1, pady=5)
        self.name_item_value.grid(row=2, column=1, pady=5)
        self.brand_item_value.grid(row=3, column=1, pady=5)
        self.price_item_value.grid(row=4, column=1, pady=5)
        self.stock_item_value.grid(row=5, column=1, pady=5)



root = Tk()

Db = NewVenda(root)

root.geometry('1366x768+0+0')
root.title('FORMULARIO DE CADASTRO')
root.mainloop()
