import datetime as dt


def titulo(tit):
    print(f'{tit:=^60}')


def op():
    op = input('=> ')
    return op
def opcao(op,tit):
    print(f'{str(op):0>2}: {tit.upper()}')

def menu_defaut(tit='',opcoes=[],sair=True):
    titulo(tit)
    if sair == False:
        for i in range(len(opcoes)):
            opcao(i,opcoes[i])
    else:
        for i in range(len(opcoes)):
            opcao(i+1,opcoes[i])
        opcao(0, 'SAIR')



    return op()

def form_defaut(tit='',opcoes=[],sair=None, DICT=False):
    titulo(tit)
    aux = []
    Daux = {}
    if sair == None:
        for i in range(len(opcoes)):
            opcao(i,opcoes[i])
            if DICT:
                Daux[opcoes[i]] = op()
            else:
                aux.append(op())

    else:
        aux.append(None)
        for i in range(len(opcoes)):

            opcao(i+1,opcoes[i])
            if DICT:
                Daux[opcoes[i]] = op()
            else:
                aux.append(op())



    return aux

def erro(msg, tipo=''):
    if tipo !='':
        return f'ERRO {tipo:0>2}: {msg} '
    else:
        return f'ERRO: {msg} '
