# -*- coding: latin-1 -*-

import manager as m
import cmd_layout as CMD
import os

ERROS = {
    0: 'Desconhecido',
    1: 'Opção Inválida',
    2: 'Estoque insuficiente',
    3: 'Produto Inexistente'
}


def sair():
    pass


def erro(i):
    print(f'ERRO [{i}]: {ERROS[i]}')


def cls():
    os.system('cls')


def verifEstoque(ID, qtd):
    if m.get_estoque(str(ID.lower()))['id'] > 0:
        if m.get_estoque(ID)['qtd'] >= qtd:
            return 0
        else:
            erro(2)
            return 1
    else:
        print(m.get_estoque(str(ID.lower())))
        erro(3)
        return 2


def novaVenda():
    produto = {}
    venda = []
    carrinho = []

    def addProd():
        opcoes = ['Nome do Produto',
                  'Quantidade'
                  ]
        return CMD.form_defaut('NOVA VENDA', opcoes, sair=True)

    def salva():
        return input('ADD e Fechar [A]    | ADD e Novo  [N]  |   Cancelar [X]')

    def fechaCaixa():
        opcoes = [
            'Data [DD/MM/AAAA]: ',
            'CLIENTE: '
        ]
        return CMD.form_defaut('FINALIZAR VENDA', opcoes, sair=True)

    op = addProd()
    salvar = salva()
    while True:
        if len(op[1].strip()) < 1:
            break
        else:
            if salvar.lower() == 'a':
                if op[2].isnumeric():
                    try:
                        produto[op[1]] = produto[op[1]] + float(op[2])
                    except KeyError:
                        produto[op[1]] = float(op[2])
                    verifica = verifEstoque(op[1], produto[op[1]])
                    if verifica == 0:
                        carrinho.append(op)
                        break
                    elif verifica == 1:
                        print(f'{op[1]} >>> {m.get_estoque(op[1])["qtd"] - aux}')
                else:
                    print('QUANTIDADE deve ser um número')

            elif salvar.lower() == 'n':
                if op[2].isnumeric():
                    try:
                        aux = produto[op[1]]
                        produto[op[1]] = produto[op[1]] + float(op[2])
                    except KeyError:
                        aux = 0
                        produto[op[1]] = float(op[2])
                    verifica = verifica = verifEstoque(op[1], produto[op[1]])
                    if verifica == 0:
                        carrinho.append(op)
                    elif verifica == 1:
                        print(f'{op[1]} >>> {m.get_estoque(op[1])["qtd"] - aux}')
                else:
                    print('QUANTIDADE deve ser um número')
            elif salvar.lower() == 'x':
                break

            else:
                erro(1)
        op = addProd()
        salvar = salva()

    if len(carrinho) > 0:
        fechar = fechaCaixa()
        for item in carrinho:
            dct = {
                'data': fechar[1],
                'origem': fechar[2],
                'descricao': item[1],
                'qtd': float(item[2]),
            }
            venda.append(dct)

    concluir = input('Salvar [S]    |   Não Salvar [N]')

    if concluir.lower() == 's':
        for item in venda:
           print( m.add_venda(item))
        print(venda)
    elif concluir.lower() == 'n':
        print('Cancelado')


def novaCompra():
    produto = {}
    compra = []
    carrinho = []

    def fechaCaixa():
        opcoes = [
            'Data DD/MM/AAAA: ',
            'Origem: ']
        return CMD.form_defaut('NOVA VENDA', opcoes, sair=True)

    def addProd():
        opcoes = [
            'Nome do Produto\n(E unidade se hover): ',
            'Quantidade: ',
            'Total: (R$)',
            'Valor de Venda (R$): '
        ]
        return CMD.form_defaut('NOVA COMPRA', opcoes, sair=True)

    def salva():
        return input('ADD e Fechar [A]    | ADD e Novo  [N]  |   Cancelar [X]')

    op = addProd()
    salvar = salva()
    while True:
        if len(op[1].strip()) < 1:
            break
        else:
            if salvar.lower() == 'a':
                if op[2].isnumeric() and op[3].isnumeric():
                    carrinho.append(op)
                    break
                else:
                    print('QUANTIDADE, TOTAL e VALOR devem ser um número')

            elif salvar.lower() == 'n':
                if op[2].isnumeric() and op[3].isnumeric() and (op[4].isnumeric() or op[4]==None):
                    carrinho.append(op)
                else:
                    print('QUANTIDADE, TOTAL e VALOR devem ser um número')
            elif salvar.lower() == 'x':
                break

            else:
                erro(1)
        op = addProd()
        salvar = salva()

    if len(carrinho) > 0:
        fechar = fechaCaixa()
        for item in carrinho:
            dct = {
                'data': fechar[1],
                'origem': fechar[2],
                'descricao': item[1],
                'qtd': float(item[2]),
                'total': float(item[3])
            }
            if len(item[4])>0:
                dct['valor_venda']=float(item[4])
            compra.append(dct)

    concluir = input('Salvar [S]    |   Não Salvar [N]')

    if concluir.lower() == 's':
        for item in compra:
            m.add_compra(item)
        print(compra)
    elif concluir.lower() == 'n':
        print('Cancelado')


def novoServico():
    produto = {}
    pecas = []
    acessorios = []

    def addDados():
        opcoes = [
            'Data DD/MM/AAAA: ',
            'Cliente: ',
            'Servico: ',
            'Mão de Obra (R$): ',
            'Materiais (R$)'
        ]
        return CMD.form_defaut('Adicionar Dados', opcoes, sair=True)

    def addPecas():
        opcoes = [
            'Nome da Peça',
            'Quantidade: ',
        ]
        return CMD.form_defaut('Adicionar Peça', opcoes, sair=True)

    def addAcessorios():
        opcoes = [
            'Nome do Acessórios',
            'Quantidade: ',
        ]
        return CMD.form_defaut('Adicionar Acessorio', opcoes, sair=True)

    def salva():
        return input('ADD e Fechar [A]    | ADD e Novo  [N]  |   Cancelar [X]')

    cls()
    CMD.titulo('NOVO SERVIÇO')

    servico = addDados()


    op = addPecas()
    salvar = salva()
    while True:
        if len(op[1].strip()) < 1:
            break
        else:
            if salvar.lower() == 'a':
                if op[2].isnumeric():
                    try:
                        produto[op[1]] = produto[op[1]] + float(op[2])
                    except KeyError:
                        produto[op[1]] = float(op[2])
                    verifica = verifEstoque(op[1], produto[op[1]])
                    if verifica == 0:
                        pecas.append(op)
                        break
                    elif verifica == 1:
                        print(f'{op[1]} >>> {m.get_estoque(op[1])["qtd"] - aux}')
                else:
                    print('QUANTIDADE deve ser um número')

            elif salvar.lower() == 'n':
                if op[2].isnumeric():
                    try:
                        aux = produto[op[1]]
                        produto[op[1]] = produto[op[1]] + float(op[2])
                    except KeyError:
                        aux = 0
                        produto[op[1]] = float(op[2])
                    verifica = verifEstoque(op[1], produto[op[1]])
                    if verifica == 0:
                        pecas.append(op)
                    elif verifica == 1:
                        print(f'{op[1]} >>> {m.get_estoque(op[1])["qtd"] - aux}')
                else:
                    print('QUANTIDADE deve ser um número')
            elif salvar.lower() == 'x':
                break

            else:
                erro(1)
        op = addPecas()
        salvar = salva()
    ###################################
    op = addAcessorios()
    salvar = salva()
    while True:
        if len(op[1].strip()) < 1:
            break
        else:
            if salvar.lower() == 'a':
                if op[2].isnumeric():
                    try:
                        produto[op[1]] = produto[op[1]] + float(op[2])
                    except KeyError:
                        produto[op[1]] = float(op[2])
                    verifica = verifEstoque(op[1], produto[op[1]])
                    if verifica == 0:
                        acessorios.append(op)
                        break
                    elif verifica == 1:
                        print(f'{op[1]} >>> {m.get_estoque(op[1])["qtd"] - aux}')
                else:
                    print('QUANTIDADE deve ser um número')

            elif salvar.lower() == 'n':
                if op[2].isnumeric():
                    try:
                        aux = produto[op[1]]
                        produto[op[1]] = produto[op[1]] + float(op[2])
                    except KeyError:
                        aux = 0
                        produto[op[1]] = float(op[2])
                    verifica = verifEstoque(op[1], produto[op[1]])
                    if verifica == 0:
                        acessorios.append(op)
                    elif verifica == 1:
                        print(f'{op[1]} >>> {m.get_estoque(op[1])["qtd"] - aux}')
                else:
                    print('QUANTIDADE deve ser um número')
            elif salvar.lower() == 'x':
                break

            else:
                erro(1)
        op = addAcessorios()
        salvar = salva()

    final = {
        'data': servico[1],
        'cliente': servico[2],
        'servico': servico[3],
        'mao_obra': float(servico[4]),
        'materiais': float(servico[5])
    }
    auxPecas = []
    for item in pecas:
        auxPecas.append([item[1],float(item[2])])

    final['pecas'] = auxPecas

    auxacessorios = ()
    for item in acessorios:
        auxacessorios.append([item[1], float(item[2])])

    final['acessorios'] = auxacessorios

    CMD.titulo('FINALIZAR SERVIÇO')
    print('Qual status do Serviço:')
    status = input('PENDENTE [P]    |   FINALIZADO [F]')
    while True:
        if status.lower()=='p':
            final['status'] = 'PEND'
            break
        elif status.lower()=='f':
            final['status'] = 'OK'
            break
        else:
            erro(1)


    concluir = input('Salvar [S]    |   Não Salvar [N]')

    if concluir.lower() == 's':
        print(m.add_servico(final))
    elif concluir.lower() == 'n':
        print('Cancelado')


def menuPrincipal():
    while True:
        cls()
        opcoes = [
            'Nova Venda',
            'Nova Compra',
            'Nova Operação',
            'Consultar'
        ]
        try:
            op = int(CMD.menu_defaut('MENU PRINCIPAL', opcoes))

        except:
            erro(1)
        if op < 0 or op > 3:
            erro(1)
        if op == 0:
            break
            sair()

        if op == 1:
            novaVenda()
        if op == 2:
            novaCompra()
        if op == 3:
            novoServico()


menuPrincipal()
