# -*- coding: latin-1 -*-

import sqlite3 as sq

erros = {
    0: "Desconhecido",
    1: "ID invalido",
    2: 'itens nao encontrados'
}

DBname = "systemTechPallasDB.db"

margemPadrao = 1.25

def c_tab(nome, tables, ID=True):
    tabelas = ''
    tabelas = tabelas + ','.join(tables)
    if ID == True:

        sql = str(
            f'CREATE TABLE IF NOT EXISTS `{nome}` (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE, {tabelas})')
    else:
        sql = str(f'CREATE TABLE IF NOT EXISTS `{nome}` ({tabelas})')
    con = sq.connect(DBname)
    c = con.cursor()
    c.execute(sql)
    con.commit()
    con.close()

    return True


def DBQUERY(SQL):
    aux = {}
    with sq.connect(DBname) as con:
        c = con.cursor().execute(SQL)
        try:
            aux = c.fetchall()
        except:
            pass
        con.commit()
        return aux


def DBQUERYONE(table, ID, col='id', field='*', DICT=False):
    sql = f'SELECT {field} FROM {table} WHERE {col}={ID}'
    with sq.connect(DBname) as con:
        value = con.cursor().execute(sql).fetchone()
    if value != None:
        if DICT:
            return to_dict(table, value)
        else:
            return value
    else:
        if DICT:
            return {'id': -1}
        else:
            return (-1,)

def DBQUERYALL(table, ID = None, col='id', field='*', DICT=False):
    if ID==None:
        sql = f'SELECT {field} FROM {table}'
    else:
        sql = f'SELECT {field} FROM {table} WHERE {col}={ID}'
    with sq.connect(DBname) as con:
        value = con.cursor().execute(sql).fetchall()
    if value != None:
        if DICT:
            return to_dict(table, value)
        else:
            return value
    else:
        if DICT:
            return {'id': -1}
        else:
            return (-1,)


def DBQUERYUPDATE(table, id, **kwargs):
    erro = []
    aux = []

    '''if get_servico(id,True)['id']>0:
        for d in kwargs.keys():
            if d not in getNameTable(table):
                erro.append(d)'''
    if len(erro) == 0:
        for v in getNameTable(table):
            print(v)
            try:
                if type(kwargs[v]) == type(''):
                    aux.append(f'{v}="{kwargs[v]}"')
                else:
                    aux.append(f'{v}={kwargs[v]}')

            except:
                print('erro')
                pass
        str = ','.join(aux).strip(',')
        sql = f'UPDATE {table} SET {str} WHERE id={id}'
        DBQUERY(sql)
        return (0, 0)
    else:
        return (1, 2, erro)


def maxID(table, field=None, condition=None):
    if type(condition) == type(''):
        campo = f'"{condition}"'
    else:
        campo = f'{condition}'
    if field == None:
        sql = f'SELECT max() FROM {table}'
    else:
        sql = f'SELECT max(id) FROM {table} WHERE {field}={campo}'
    with sq.connect(DBname) as con:
        value = con.cursor().execute(sql).fetchone()
        return value


def getNameTable(table):
    col = []
    sql = f'SELECT * FROM {table} WHERE id=0'
    v = sq.connect(DBname).execute(sql).description
    for i in v:
        col.append(i[0])
    return col


def to_dict(table, value=[]):
    try:
        c = getNameTable(table)
        d = {}
        for v in range(len(value)):
            d[c[v]] = value[v]

        return d
    except:
        return {'id': -1}


def TB_servicos():
    col = [
        'data TEXT',
        'cliente TEXT',
        'servico TEXT',
        'pecas TEXT',
        'valor_pecas REAL',
        'acessorios INTEGER',
        'valor_acessorios REAL',
        'mao_obra REAL',
        'materias REAL',
        'total REAL',
        'status TEXT',
    ]
    c_tab('servicos', col)


def TB_estoque():
    col = [
        'item TEXT',
        'qtd REAL',
        'unidade TEXT',
        'valor_unitario REAL',
        'valor_venda REAL',
    ]
    c_tab('estoque', col)


def TB_comp_vend():
    col = [
        'data TEXT',
        'origem TEXT',
        'descricao TEXT',
        'tipo TEXT',
        'qtd REAL',
        'valor',
        'status TEXT',

    ]
    c_tab('comp_vend', col)


def TB_clientes():
    col = [
        'nome TEXT',
        'telefone TEXT',
        'email TEXT',
        'local TEXT',
    ]
    c_tab('clientes', col)


def TB_operacoes():
    col = [
        'data TEXT',
        'descricao TEXT',
        'tipo TEXT',
        'orig_dest TEXT',
        'credito REAL',
        'debito REAL'
    ]
    c_tab('operacoes', col)


def CreateDB():
    TB_operacoes()
    TB_servicos()
    TB_comp_vend()
    TB_estoque()
    TB_clientes()


CreateDB()


######################PECAS##############################
def add_estoque(item, qtd, unidade, VU, valor_venda=None):
    ID = get_estoque(item, True)['id']
    VD = VU*margemPadrao
    if valor_venda!=None:
        VD = valor_venda
    if ID == -1:
        sql = f'INSERT INTO estoque (item,qtd,unidade,valor_unitario, valor_venda) VALUES ("{item.lower()}",{float(qtd)},"{unidade.lower()}",{float(VU)},{float(VD)})'
        DBQUERY(sql)

    else:
        oldQtd = get_estoque(ID, True)['qtd']
        oldVU = get_estoque(ID, True)['valor_unitario']

        nVU = ((oldVU * oldQtd) + (VU * qtd)) / (qtd + oldQtd)
        nQtd = (qtd + oldQtd)

        ret = DBQUERYUPDATE('estoque', ID, item=item.lower(), qtd=nQtd, unidade=unidade.lower(), valor_unitario=nVU)
        return (0, 0, ret)


def get_estoque(id, DICT=False):
    if type(id) == type(0):
        return DBQUERYONE('estoque', id, DICT=DICT)

    if type(id) == type(''):
        return DBQUERYONE('estoque', f'"{id.lower()}"', col='item', DICT=DICT)

    else:
        if DICT:
            return {'id': 0, 'item': "ERRO: ID invalido"}

        else:
            return (0, "ERRO: ID invalido")


def update_estoque(id, item=None, qtd=None, unidade=None, VU=None):
    aux = []
    if get_estoque(id)[0] > 0:
        if item != None:
            aux.append(f'item="{item}"')
        if qtd != None:
            aux.append(f'qtd={int(qtd)}')
        if unidade != None:
            aux.append(f'unidade="{unidade}"')
        if VU != None:
            aux.append(f'valor_unitario={float(VU)}')

        str = ','.join(aux).strip(',')

        sql = f'UPDATE estoque SET {str} WHERE id={id}'
        DBQUERY(sql)
        return (0, 0)
    else:
        return (1, 1)




#########################################################

def add_servico(data, cliente, servico, pecas , acessorios, mao_obra, materiais, status='PEND'):
    tpecas = ''
    tacessorios = ''
    valor_acessorios = 0
    valor_pecas = 0
    erro = []
    for peca in pecas:
        # cada peca eh um vetor de dois espacos [nome,qtd]
        if get_estoque(peca[0], DICT=True)['id'] > 0:
            tpecas = f'{tpecas}/{peca[0]}:{peca[1]}'
            valor_pecas = valor_pecas + float(get_estoque(peca[0], DICT=True)['valor_unitario'] * peca[1])
        else:
            erro.append(peca[0])

    tpecas = tpecas.strip('/')

    for acessorio in acessorios:
        if get_estoque(acessorio[0], DICT=True)['id'] > 0:
            tacessorios = f'{tacessorios}/{acessorio[0]}:{acessorio[1]}'
            valor_acessorios = valor_acessorios + float(
                get_estoque(acessorio[0], DICT=True)['valor_unitario'] * acessorio[1])
        else:
            erro.append(acessorio[0])
    tacessorios = tacessorios.strip('/')

    total = valor_acessorios + valor_pecas + materiais + mao_obra

    sql = f'''INSERT INTO servicos (data,cliente,servico,pecas,valor_pecas,acessorios,valor_acessorios,mao_obra,materias,total,status) 
    VALUES ("{data}","{cliente.lower()}","{servico.lower()}","{tpecas}",{valor_pecas},"{tacessorios}",{valor_acessorios},{float(
        mao_obra)},{float(materiais)},{float(total)},"{status.upper()}")'''
    if len(erro) == 0:
        DBQUERY(sql)
        return (0, 0)
    else:
        return (1, 2, erro)


def get_servico(id, DICT=False):
    return DBQUERYONE('servicos', id, DICT=DICT)


def update_servico(id, **col):
    return DBQUERYUPDATE('servicos', id, **col)


###############################################################
def add_comp_vend(data, origem, descricao, tipo, qtd, total):
    sql = f'INSERT INTO comp_vend (data,origem,descricao,tipo,qtd,valor) VALUES ("{data}","{origem}","{descricao}","{tipo}",{qtd},{total})'
    DBQUERY(sql)
    return (0, 0)


def get_comp_vend(id, DICT=False):
    return DBQUERYONE('comp_vend', id, DICT=DICT)


def update_comp_vend(id, **col):
    return DBQUERYUPDATE('comp_vend', id, **col)


###############################################################
def add_cliente(nome, telefone, email, local):
    sql = f'INSERT INTO clientes (nome, telefone, email, local) VALUES ("{nome}","{telefone}","{email}","{local}")'
    DBQUERY(sql)
    return (0, 0)


def get_cliente(id, DICT=False):
    return DBQUERYONE('clientes', id, DICT=DICT)


def update_cliente(id, **col):
    return DBQUERYUPDATE('clientes', id, **col)


###############################################################
def add_operacoes(data, descricao, tipo, orig_dest, credito, debito):
    sql = f'INSERT INTO operacoes (data, descricao, tipo, orig_dest, credito, debito) VALUES ("{data}", "{descricao}", "{tipo}", "{orig_dest}", {credito}, {debito})'
    DBQUERY(sql)
    return (0, 0)


def get_operacao(id, DICT=False):
    return DBQUERYONE('operacoes', id, DICT=DICT)


def update_operacao(id, **col):
    return DBQUERYUPDATE('operacoes', id, **col)


###############################################################


def teste_add_pecas():
    add_estoque('Conector', 4, 'UNIDADE', 3)
    add_estoque('FIO', 4, 'UNIDADE', 5)
    add_estoque('cabo', 4, 'UNIDADE', 7)
    add_estoque('tela', 4, 'UNIDADE', 8)
    add_estoque('carregador', 4, 'UNIDADE', 9)


# teste_add_pecas()
pecas = [[1, 2], [2, 3], [3, 4]]
acessorios = [[4, 5], [5, 6]]
# print(add_servico('10/10/2018','patrick','Teste',pecas,acessorios,45,57))
# print(update_servico(2,data='10/12/12',mao_obra=8))




