# -*- coding: latin-1 -*-

import DBmanager as DB

ERROS = {
    0: 'Desconhecido',
    1:'estoque insuficiente',
    2:'ID inexistente',
}

FLOAT = type(1.75)
INT = type(0)
STRING = type('')
BOOL = type(True)
ARRAY = type([])

def convUnit(v,unidade):
    unidades = {'g':[1,'g'],
               'kg':[1000,'g'],
               'l':[1,'l'],
               'ml': [1/1000, 'l'],
               'm': [1,'m'],
               'cm':[1/1000,'m']
               }
    if unidade in unidades.keys():
        return (v*unidades[unidade][0],unidades[unidade][1])
    else:
        return (v,unidade)


def verifica(atributos, dados):
    '''

    :param atributos: Dicionario com nome dos atributos necessarios e o tipo do mesmo
    :param dados: Dicionario com os dados a serem verificados
    :return: True se os todos os atributos necessarios estao contidos em dados
    '''
    def array():
        if len(atributos[atributo]) == len(dados[atributo]):
            for i in range(len(dados[atributo])):
                if type(dados[atributo][i]) != atributos[atributo][i]:
                    try:
                        aux = atributos[atributo](dados[atributo])
                    except:
                        ok = False
    erro = 0
    ok = True
    # verifica se dados eh um dicionario
    if type(dados) == type({}):
        # verifica se os atributos.keys()
        for atributo in atributos.keys():
            if atributo not in dados.keys():
                ok = False
                break
            else:
                if atributos[atributo] == ARRAY:
                    array()


                if type(dados[atributo]) != atributos[atributo]:
                    try:
                        aux = atributos[atributo](dados[atributo])
                    except:
                        ok = False

    return ok


def add_estoque(dados):
    erro = 0
    atributos = {'item': STRING, 'qtd': FLOAT, 'unidade': STRING, 'VU': FLOAT}
    VD = None
    try:
        VD = dados['valor_venda']
    except KeyError:
        pass
    if verifica(atributos, dados):
        print('sim')
        ret = DB.add_estoque(dados['item'], dados['qtd'], dados['unidade'], dados['VU'],valor_venda=VD)
        return ret
        print(ret)
    else:
        print('nao')


def get_estoque(id):
    return DB.get_estoque(id, True)


def add_compra(d):
    atributos = {'data': STRING, 'origem': STRING, 'descricao': STRING, 'qtd': FLOAT, 'total': FLOAT}
    VD = None
    try:
        VD = d['valor_venda']
    except KeyError:
        pass
    if verifica(atributos, d):
        DB.add_comp_vend(d['data'], d['origem'], d['descricao'], 'COMPRA', d['qtd'], d['total'])

        try:
            item = d['descricao'].split('/')[0]
            unidade = d['descricao'].split('/')[1]
        except:
            item = d['descricao']
            unidade = 'unidade'

        d_estq = {'item': item,
                  'qtd': convUnit(d['qtd'],unidade)[0],
                  'unidade': convUnit(d['qtd'],unidade)[1],
                  'VU': d['total'] /convUnit(d['qtd'],unidade)[0],
                  'valor_venda':VD}

        add_estoque(d_estq)

        if DB.maxID('comp_vend', 'tipo', 'COMPRA')[0] > 0:
            IDCV = str('IDCV' + str(DB.maxID('comp_vend', 'tipo', 'COMPRA')[0]))

        d_op = {'data': d['data'],
                'descricao': IDCV,
                'tipo': 'COMPRA',
                'orig_dest': d['origem'],
                'credito': 0,
                'debito': d['total']}

        add_operacao(d_op)



def add_venda(d):
    atributos = {'data': STRING, 'origem': STRING, 'descricao': STRING, 'qtd': FLOAT}
    if verifica(atributos, d):

        try:
            item = d['descricao'].split('/')[0]
            unidade = d['descricao'].split('/')[1]
        except:
            item = d['descricao']
            unidade = 'unidade'
        if get_estoque(item)['qtd']>=d['qtd']:
            total = d['qtd']*get_estoque(item)['valor_venda']


            DB.add_comp_vend(d['data'], d['origem'], d['descricao'], 'VENDA', d['qtd'],total)

            d_estq = {'item': item,
                      'qtd': 0-convUnit(d['qtd'],unidade)[0]
                      }
            add_estoque(d_estq)
            if DB.maxID('comp_vend', 'tipo', 'VENDA')[0] > 0:

                IDCV = str('IDCV' + str(DB.maxID('comp_vend', 'tipo', 'VENDA')[0]))

                d_op = {'data': d['data'],
                        'descricao': IDCV,
                        'tipo': 'VENDA',
                        'orig_dest': d['origem'],
                        'credito': total,
                        'debito': 0 }

                add_operacao(d_op)
            else:
                (1,2)

        else:
            return (1,1,item)


def get_comp_vend(ID):
    return DB.get_comp_vend(ID, True)

def add_operacao(d):
    atributos = {'data': STRING,
                 'descricao': STRING,
                 'tipo': STRING,
                 'orig_dest': STRING,
                 'credito': FLOAT,
                 'debito': FLOAT}
    if verifica(atributos, d):
        DB.add_operacoes(d['data'], d['descricao'], d['tipo'], d['orig_dest'], d['credito'], d['debito'])

        def add_venda(d):
            atributos = {'data': STRING, 'origem': STRING, 'descricao': STRING, 'qtd': FLOAT, 'total': FLOAT}
            if verifica(atributos, d):

                try:
                    item = d['descricao'].split('/')[0]
                    unidade = d['descricao'].split('/')[1]
                except:
                    item = d['descricao']
                    unidade = 'unidade'

                ID = get_estoque(item)

                if ID['id'] > 0:
                    DB.add_comp_venda(d['data'], d['origem'], d['descricao'], 'VENDA', d['qtd'], d['total'])
                    d_estq = {'item': item,
                              'qtd': 0 - d['qtd'],
                              'unidade': unidade,
                              'VU': d['total'] / d['qtd']}
                    add_estoque(ID['id'], qtd=0 - d['qtd'])

                    d_op = {'data': d['data'],
                            'descricao': d['descricao'],
                            'tipo': 'VENDA',
                            'orig_dest': d['origem'],
                            'credito': d['total'],
                            'debito': 0}

                    add_operacao(d_op)


def get_operacao(ID):
    return DB.get_operacao(ID, True)

def add_servico(d):
    atributos = {
    'data': STRING,
    'cliete': STRING,
    'servico': STRING,
    'pecas': [[STRING,FLOAT]],
    'acessorios': [[STRING,FLOAT]],
    'mao_obra': FLOAT,
    'materiais': FLOAT,
    'status': STRING}
    erro = []
    tpecas = []
    tacess = []
    total = 0
    ok = True
    if verifica(atributos, d)==False:
        # verificacao de estoque:
        for peca in d['pecas']:
            d_estq = {
                'item':peca[0],
                'qtd':0-float(peca[1]),
            }
            if peca[1]> get_estoque(peca[0])['qtd']:
                ok = False
                erro.append(d_estq)
            else:
                tpecas.append(d_estq)
                total = total +  get_estoque(peca[0])['valor_unitario']*peca[1]

        for acessorio in d['acessorios']:
            d_acess = {
                    'item': acessorio[0],
                    'qtd': 0 - acessorio[1],
                }
            if acessorio[1]>get_estoque(acessorio[0])['qtd']:
                erro.append(d_acess)
                ok=False
            else:
                tacess.append(d_acess)
                total = total + get_estoque(acessorio[0])['valor_unitario'] * acessorio[1]
        if ok:
            total = total + d['mao_obra'] + d['materiais']
            try:
                status = d['status']
            except KeyError:
                status = 'PEND'
            DB.add_servico(d['data'], d['cliente'], d['servico'], d['pecas'], d['acessorios'], d['mao_obra'],d['materiais'], status)

            for peca in tpecas:
                add_estoque(peca)

            for acessorio in tacess:
                add_estoque(acessorio)
            IDS = str('IDS' + str(DB.maxID('servicos','cliente',d['cliente'].lower())[0]))
            d_op = {'data': d['data'],
                    'descricao': IDS,
                    'tipo': 'SERVICO',
                    'orig_dest': d['cliente'],
                    'credito': total,
                    'debito': 0}
            add_operacao(d_op)
            return (0,0)
        else:
            (1,1,erro)
    else:
        return ('Erro 2')


def get_servico(ID):
    return DB.get_estoque(ID, True)

def get_all_compra():
    aux = []
    for i in DB.DBQUERYALL('comp_vend','"COMPRA"','tipo', DICT=True).values():
        aux.append(i)
    return aux
def get_all_venda():
    aux = []
    for i in DB.DBQUERYALL('comp_vend', '"VENDA"', 'tipo', DICT=True).values():
        aux.append(i)
    return aux
def get_all_estoque():
    aux = []
    for i in DB.DBQUERYALL('comp_vend', DICT=True).values():
        aux.append(i)
    return aux


def testeDB(adds=False,gets=True):
    compras = [
        {'data': '30/03/2019',
         'origem': 'Fornecedor Teste',
         'descricao': 'Tela J5',
         'qtd': 3,
         'total': 30
         },
        {'data': '30/03/2019',
         'origem': 'Fornecedor Teste',
         'descricao': 'Conector J5',
         'qtd': 3,
         'total': 10,
         'valor_venda': 5
         },
        {'data': '30/03/2019',
         'origem': 'Fornecedor Teste',
         'descricao': 'Solda/kg',
         'qtd': 0.5,
         'total': 15
         },

    ]
    servico = {
    'data': '30/03/2019',
    'cliente': 'Patrick',
    'servico': 'troca de tela J5',
    'pecas': [['Tela J5',1]],
    'acessorios': [['Conector J5',1]],
    'mao_obra': 50,
    'materiais': 5,
    'status': 'OK'
    }

    venda = {'data': '30/03/2019',
         'origem': 'Fornecedor Teste',
         'descricao': 'Conector J5',
         'qtd': 2,
         }
    if adds:
        for compra in compras:
            add_compra(compra)

        print(add_servico(servico))
        print(add_venda(venda))
        print('fim')

    if gets:
        print('COMPRA', get_comp_vend(1))
        print('VENDA', get_comp_vend(4))
        print('ESTOQUE', get_estoque(2))
        print('OPERACAO', get_operacao(5))



#testeDB(False,True)
